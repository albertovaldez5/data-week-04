#+title:    Python Cheatsheet
#+subtitle: Python and Pandas
#+author: Alberto Valdez
#+SETUPFILE: ../config/org-theme-alt.config
#+SETUPFILE: ../config/org-header.config

* Python Cheatsheet

** File paths with os

#+begin_src python
import os

path = os.getcwd()
csv_file = os.path.join(path, "")
print(csv_file)
#+end_src

#+RESULTS[5f722664586ef299150ef1d040a0d365b17c9ec9]:
#+begin_example
/Users/albertovaldez/data-notes/resources/week-04/src
#+end_example

** File paths with pathlib

#+begin_src python
from pathlib import Path

path = Path.cwd() / "50-cheatsheets"
csv_file = path / "schools.csv"
print(csv_file)
#+end_src

#+RESULTS:
#+begin_src org
/Users/albertovaldez/data-analytics-notes/src/50-cheatsheets/schools.csv
#+end_src

** Reading and writing files
#+begin_src python
my_file = path / "output.txt"
my_data = "Hello World!"
# Writing data to text file
with open(my_file, "w+") as f:
    f.write(my_data)
# Reading data from text file
with open(my_file, "r") as f:
    read_data = f.read()
print(read_data)
#+end_src

#+RESULTS:
#+begin_src org
Hello World!
#+end_src

* Lists, Sets, Dictionaries
** Lists comprehensions
#+begin_src python
my_list = ["Bob", "Alice", "Clark Kent", "Dr. Gregory House", "Dr. Lisa Cuddy", "ARM Dwight Schrute"]
# Regular list
doctors_list = []
for name in my_list:
    if "Dr." in name:
        doctors_list.append(name)
print(doctors_list)
# The same for loop but in a list comprehension
doctors_list2 = [name for name in my_list if "Dr." in name]
print(doctors_list2)
#+end_src

#+RESULTS:
#+begin_src org
['Dr. Gregory House', 'Dr. Lisa Cuddy']
['Dr. Gregory House', 'Dr. Lisa Cuddy']
#+end_src

** Removing duplicates with Set
#+begin_src python
titles = [name.split()[0] for name in doctors_list]
print(titles)
print(set(titles))
#+end_src

#+RESULTS:
#+begin_src org
['Dr.', 'Dr.']
{'Dr.'}
#+end_src

** Iterating over multiple Lists
#+begin_src python
teams_east = ["Brooklyn Nets", "Miami Heat", "Boston Celtics"]
teams_west = ["Golden State Warriors", "Utah Jazz", "San Antonio Spurs", "Houston Rockets"]

for east, west in zip(teams_east, teams_west):
    print(f"{east} v.s. {west}")
#+end_src

#+RESULTS:
#+begin_src org
Brooklyn Nets v.s. Golden State Warriors
Miami Heat v.s. Utah Jazz
Boston Celtics v.s. San Antonio Spurs
#+end_src

** Enumerating lists

#+begin_src python
for i, team in enumerate(teams_east):
    print(f"{i+1}. {team}")
#+end_src

#+RESULTS:
#+begin_src org
1. Brooklyn Nets
2. Miami Heat
3. Boston Celtics
#+end_src

* String Manipulation
** Splitting a string into a list
#+begin_src python
full_name = "Dr. Gregory House"
full_name_split = dr_name.split()
print(full_name_split)
print(full_name_split[-1])
#+end_src

#+RESULTS:
#+begin_src org
#+end_src
** Strip a string from a string
#+begin_src python
name = full_name.strip("Dr. ")
print(name)
#+end_src

#+RESULTS:
#+begin_src org
Gregory House
#+end_src
** Replace a string in a string
#+begin_src python
name = full_name.replace("Dr.", "Mr.")
print(name)
#+end_src

#+RESULTS:
#+begin_src org
Mr. Gregory House
#+end_src

** Join a list into a string
#+begin_src python
dog_breeds = ["Bulldog", "German Sheperd", "Poodle", "Labrador Retriever", "Pug"]
new_string = ", ".join(i for i in dog_breeds)
print(new_string)
#+end_src

#+RESULTS:
#+begin_src org
Bulldog, German Sheperd, Poodle, Labrador Retriever, Pug
#+end_src

* Printing and Formatting
** F-Strings (requires Python >= 3.6)
#+begin_src python
number = 384038
total = 500000
print(f"Number: {number:,}. Total: {total:.2f}")
print(f"Percentage: {100*number/total:.2f}%")
#+end_src

#+RESULTS:
#+begin_src org
Number: 384,038. Total: 500000.00
Percentage: 76.81%
#+end_src

* Classes
** Declaring a class
#+begin_src python
class Cat:
    def __init__(self, name, color = "black"):
        """Create a new cat, give it a name."""
        self.name = name
        self.color = color
        self.mood = "happy"

    def meow(self):
        """Make the cat meow."""
        return f"{self.name} meows!"

    def is_happy(self):
        """If the cat is happy, return True."""
        if self.mood == "happy":
            return True
        return False

    def __add__(self, other_cat):
        return f"{self.name} and {other_cat.name}"

    def __repr__(self):
        return f"""
This is {self.name}:
   \    /\\
    )  ( `)
   (  /  )
    \(__)|"""
print(Cat)
#+end_src

#+RESULTS:
#+begin_src org
<class '__main__.Cat'>
#+end_src

** Creating Instances of a Class
#+begin_src python
oreo_cat = Cat("Oreo")
lucy_cat = Cat("Lucy", color = "white")
my_cats = [oreo_cat, lucy_cat]
print(my_cats)
#+end_src

#+RESULTS:
#+begin_src org
[
This is Oreo:
   \    /\
    )  ( `)
   (  /  )
    \(__)|,
This is Lucy:
   \    /\
    )  ( `)
   (  /  )
    \(__)|]
#+end_src

** Accessing Attributes and Methods of a Class
#+begin_src python
print(f"My cats are {oreo_cat + lucy_cat}.")
print(oreo_cat.meow())
for cat in my_cats:
    print(f"{cat.name}'s color is {cat.color}.")
    if cat.is_happy():
        print(f"{cat.name} is happy.")
#+end_src

#+RESULTS:
#+begin_src org
My cats are Oreo and Lucy.
Oreo meows!
Oreo's color is black.
Oreo is happy.
Lucy's color is white.
Lucy is happy.
#+end_src

