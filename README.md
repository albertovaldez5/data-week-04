- [Pandas Basics](#pandas-basics)
  - [Creating DataFrames](#creating-dataframes)
  - [Reading file into DataFrame](#reading-file-into-dataframe)
  - [Describing the DataFrame](#describing-the-dataframe)
  - [Setting the Index](#setting-the-index)
  - [Renaming a Column](#renaming-a-column)
  - [Renaming Multiple Columns](#renaming-multiple-columns)
  - [Getting Types of Values in Columns](#getting-types-of-values-in-columns)
- [Pandas Operations](#pandas-operations)
  - [Counting Values in a DataFrame](#counting-values-in-a-dataframe)
  - [Counting Unique Values in a Series](#counting-unique-values-in-a-series)
  - [Sorting Values](#sorting-values)
  - [Finding Empty Values](#finding-empty-values)
  - [Finding Non-Empty Values](#finding-non-empty-values)
  - [Deleting rows with missing data](#deleting-rows-with-missing-data)
  - [Adding missing data to rows](#adding-missing-data-to-rows)
  - [Removing Duplicates](#removing-duplicates)
- [Pandas Transformations](#pandas-transformations)
  - [Converting data to a List](#converting-data-to-a-list)
  - [Merging DataFrames](#merging-dataframes)
  - [Finding Values that pass a Condition](#finding-values-that-pass-a-condition)
  - [Getting the Values that pass a Condition](#getting-the-values-that-pass-a-condition)
  - [Getting Values with Multiple Conditions](#getting-values-with-multiple-conditions)
  - [Applying a function to all Values in a Column](#applying-a-function-to-all-values-in-a-column)
  - [Grouping Values by Different Operations](#grouping-values-by-different-operations)
  - [Grouping the Data in Bins (Binning)](#grouping-the-data-in-bins-binning)



<a id="pandas-basics"></a>

# Pandas Basics

```python
import pandas as pd
```


<a id="creating-dataframes"></a>

## Creating DataFrames

```python
my_dict = {
    "Frame": [0, 0, 1920, 1080],
    "Labels": ["x", "y", "w", "h"],
}
my_dataframe = pd.DataFrame(my_dict)
print(my_dataframe)
```

```
   Frame Labels
0      0      x
1      0      y
2   1920      w
3   1080      h
```


<a id="reading-file-into-dataframe"></a>

## Reading file into DataFrame

```python
from pathlib import Path

csv_file = Path("../resources/schools_complete.csv")
school_df = pd.read_csv(csv_file)
print("Head:\n", school_df.head(2))
print("Tail:\n", school_df.tail(2))
```

```
Head:
    School ID           school_name      type  size   budget
0          0     Huang High School  District  2917  1910635
1          1  Figueroa High School  District  2949  1884411
Tail:
     School ID         school_name      type  size   budget
13         13    Ford High School  District  2739  1763916
14         14  Thomas High School   Charter  1635  1043130
```


<a id="describing-the-dataframe"></a>

## Describing the DataFrame

```python
print(school_df.describe())
```

```
       School ID         size        budget
count  15.000000    15.000000  1.500000e+01
mean    7.000000  2611.333333  1.643295e+06
std     4.472136  1420.915282  9.347763e+05
min     0.000000   427.000000  2.480870e+05
25%     3.500000  1698.000000  1.046265e+06
50%     7.000000  2283.000000  1.319574e+06
75%    10.500000  3474.000000  2.228999e+06
max    14.000000  4976.000000  3.124928e+06
```


<a id="setting-the-index"></a>

## Setting the Index

```python
school_df_indexed = school_df.set_index("School ID")
print(school_df_indexed.index)
print(school_df_indexed.head(3))
```

```
Int64Index([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], dtype='int64', name='School ID')
                    school_name      type  size   budget
School ID
0             Huang High School  District  2917  1910635
1          Figueroa High School  District  2949  1884411
2           Shelton High School   Charter  1761  1056600
```


<a id="renaming-a-column"></a>

## Renaming a Column

```python
school_df_renamed = school_df_indexed.rename(columns = {
"school_name": "School Name"})
print(school_df_renamed.head(3))
```

```
                    School Name      type  size   budget
School ID
0             Huang High School  District  2917  1910635
1          Figueroa High School  District  2949  1884411
2           Shelton High School   Charter  1761  1056600
```


<a id="renaming-multiple-columns"></a>

## Renaming Multiple Columns

```python
key_value = {
    "school_name":"School Name",
    "type":"School Type",
    "size":"School Size",
    "budget":"School Budget"
}
school_df_renamed_2 = school_df_indexed.rename(columns=key_value)
print(school_df_renamed_2.columns)
```

```
Index(['School Name', 'School Type', 'School Size', 'School Budget'], dtype='object')
```


<a id="getting-types-of-values-in-columns"></a>

## Getting Types of Values in Columns

```python
print(school_df_renamed.dtypes)
print(f"Budget type: {school_df_renamed['budget'].dtype}")
```

```
School Name    object
type           object
size            int64
budget          int64
dtype: object
Budget type: int64
```


<a id="pandas-operations"></a>

# Pandas Operations


<a id="counting-values-in-a-dataframe"></a>

## Counting Values in a DataFrame

```python
print(school_df.count())
```

```
School ID      15
school_name    15
type           15
size           15
budget         15
dtype: int64
```


<a id="counting-unique-values-in-a-series"></a>

## Counting Unique Values in a Series

```python
df = pd.DataFrame(
    {"values":["a", "b", "a", "c", "b", "b"]}
)
print(df["values"].value_counts())
```

```
b    3
a    2
c    1
Name: values, dtype: int64
```


<a id="sorting-values"></a>

## Sorting Values

```python
df = pd.DataFrame(
    {
        "names":["Bob", "Alice", "John", "Katy", "Alex"],
        "values":[5,3,6,0,1],
    }
)
sorted_df = df.sort_values(["values"], ascending=False)
print(sorted_df.head(3))
```

```
   names  values
2   John       6
0    Bob       5
1  Alice       3
```


<a id="finding-empty-values"></a>

## Finding Empty Values

```python
print(school_df.isnull().head())
print(school_df.isnull().sum())
```

```
   School ID  school_name   type   size  budget
0      False        False  False  False   False
1      False        False  False  False   False
2      False        False  False  False   False
3      False        False  False  False   False
4      False        False  False  False   False
School ID      0
school_name    0
type           0
size           0
budget         0
dtype: int64
```


<a id="finding-non-empty-values"></a>

## Finding Non-Empty Values

```python
print(school_df.notnull().head())
print(school_df.notnull().sum())
```

```
   School ID  school_name  type  size  budget
0       True         True  True  True    True
1       True         True  True  True    True
2       True         True  True  True    True
3       True         True  True  True    True
4       True         True  True  True    True
School ID      15
school_name    15
type           15
size           15
budget         15
dtype: int64
```


<a id="deleting-rows-with-missing-data"></a>

## Deleting rows with missing data

```python
df = pd.read_csv(Path("../resources/missing_grades.csv"))
print(df.head())
```

```
   Student ID       student_name gender grade  reading_score  math_score
0           0       Paul Bradley      M   9th           66.0        79.0
1           1       Victor Smith      M  12th           94.0        61.0
2           2    Kevin Rodriguez      M  12th            NaN        60.0
3           3  Dr. Richard Scott      M  12th           67.0        58.0
4           4         Bonnie Ray      F   9th           97.0        84.0
```

```python
df_drop = df.dropna()
print(df_drop.head())
```

```
   Student ID       student_name gender grade  reading_score  math_score
0           0       Paul Bradley      M   9th           66.0        79.0
1           1       Victor Smith      M  12th           94.0        61.0
3           3  Dr. Richard Scott      M  12th           67.0        58.0
4           4         Bonnie Ray      F   9th           97.0        84.0
6           6      Sheena Carter      F  11th           82.0        80.0
```


<a id="adding-missing-data-to-rows"></a>

## Adding missing data to rows

```python
df_fill = df.fillna(85)
print(df_fill.head(3))
```

```
   Student ID     student_name gender grade  reading_score  math_score
0           0     Paul Bradley      M   9th           66.0        79.0
1           1     Victor Smith      M  12th           94.0        61.0
2           2  Kevin Rodriguez      M  12th           85.0        60.0
```


<a id="removing-duplicates"></a>

## Removing Duplicates

```python
df = pd.DataFrame({
    "planet":["Mars", "Jupiter", "Mars", "Mercury", "Mars"],
    "radius (km)": [3390, 69911, 3390, 2440, 3930]
})
print(df.drop_duplicates())
```

```
    planet  radius (km)
0     Mars         3390
1  Jupiter        69911
3  Mercury         2440
4     Mars         3930
```


<a id="pandas-transformations"></a>

# Pandas Transformations


<a id="converting-data-to-a-list"></a>

## Converting data to a List

```python
students_df = pd.read_csv(Path("../resources/students_complete.csv"))
data_list = students_df["student_name"].tolist()
print(data_list[0:5])
```

```
['Paul Bradley', 'Victor Smith', 'Kevin Rodriguez', 'Dr. Richard Scott', 'Bonnie Ray']
```


<a id="merging-dataframes"></a>

## Merging DataFrames

```python
schools_df = pd.read_csv(Path("../resources/schools_complete.csv"))
merged_df = pd.merge(students_df, schools_df, on=["school_name", "school_name"])
print(merged_df.head())
```

```
   Student ID       student_name gender grade        school_name  ...  math_score  School ID      type  size   budget
0           0       Paul Bradley      M   9th  Huang High School  ...          79          0  District  2917  1910635
1           1       Victor Smith      M  12th  Huang High School  ...          61          0  District  2917  1910635
2           2    Kevin Rodriguez      M  12th  Huang High School  ...          60          0  District  2917  1910635
3           3  Dr. Richard Scott      M  12th  Huang High School  ...          58          0  District  2917  1910635
4           4         Bonnie Ray      F   9th  Huang High School  ...          84          0  District  2917  1910635

[5 rows x 11 columns]
```


<a id="finding-values-that-pass-a-condition"></a>

## Finding Values that pass a Condition

```python
passed_math_index = students_df["math_score"] >= 70
print(passed_math_index.head())
```

```
0     True
1    False
2    False
3    False
4     True
Name: math_score, dtype: bool
```


<a id="getting-the-values-that-pass-a-condition"></a>

## Getting the Values that pass a Condition

```python
passed_math = students_df[students_df["math_score"] >= 70]
print(passed_math.head())
```

```
   Student ID   student_name gender grade        school_name  reading_score  math_score
0           0   Paul Bradley      M   9th  Huang High School             66          79
4           4     Bonnie Ray      F   9th  Huang High School             97          84
5           5  Bryan Miranda      M   9th  Huang High School             94          94
6           6  Sheena Carter      F  11th  Huang High School             82          80
8           8   Michael Roth      M  10th  Huang High School             95          87
```


<a id="getting-values-with-multiple-conditions"></a>

## Getting Values with Multiple Conditions

```python
passed_overall = students_df[
    (students_df["math_score"] >= 70)
    & (students_df["reading_score"] >= 70)
]
print(passed_overall.head())
```

```
   Student ID    student_name gender grade        school_name  reading_score  math_score
4           4      Bonnie Ray      F   9th  Huang High School             97          84
5           5   Bryan Miranda      M   9th  Huang High School             94          94
6           6   Sheena Carter      F  11th  Huang High School             82          80
8           8    Michael Roth      M  10th  Huang High School             95          87
9           9  Matthew Greene      M  10th  Huang High School             96          84
```


<a id="applying-a-function-to-all-values-in-a-column"></a>

## Applying a function to all Values in a Column

```python
df = pd.DataFrame({"USD":[45, 99, 32, 55]})
df_total = df["USD"].sum()

def get_percentage(value):
    return f"{100 * value/df_total:,.2f}%"

convert_to_mxn = lambda x: f"${20.88*x:,.2f}"

df["Percentage"] = df["USD"].map(get_percentage)
df["MXN"] = df["USD"].map(convert_to_mxn)
df["USD"] = df["USD"].map("${:.1f}".format)

print(df)
```

```
     USD Percentage        MXN
0  $45.0     19.48%    $939.60
1  $99.0     42.86%  $2,067.12
2  $32.0     13.85%    $668.16
3  $55.0     23.81%  $1,148.40
```


<a id="grouping-values-by-different-operations"></a>

## Grouping Values by Different Operations

```python
grouped = students_df.groupby(["school_name"])
grouped_mean = grouped.mean()["reading_score"]
print(grouped_mean.head())
```

```
school_name
Bailey High School      81.033963
Cabrera High School     83.975780
Figueroa High School    81.158020
Ford High School        80.746258
Griffin High School     83.816757
Name: reading_score, dtype: float64
```


<a id="grouping-the-data-in-bins-binning"></a>

## Grouping the Data in Bins (Binning)

```python
bins = [0, 100, 500, 1000]
bin_names = ["Low", "Mid", "High"]
series = pd.Series({"A":419, "B": 667, "C": 100, "D": 900, "E":1001})
binned_df = pd.cut(series, bins, labels = bin_names)
print(binned_df.head())
```

```
A     Mid
B    High
C     Low
D    High
E     NaN
dtype: category
Categories (3, object): ['Low' < 'Mid' < 'High']
```